import { FETCH_COUNTRIES } from "../actions/index";

export default function(state = [], action) {
  function countryExists(countryData) {
    return countryData.name == action.payload.data.name;
  }

  function filterCountries(countryData) {
    return countryData.name != action.payload.data.name;
  }

  switch (action.type) {
    case FETCH_COUNTRIES:
    if(action.payload.data.status != '404') {
      if (state.find(countryExists)) {
        const filteredCountries = state.filter(filterCountries);
        return [action.payload.data[0], ...filteredCountries];
      }
      else {
        return [action.payload.data[0], ...state];
      }
    }
  }
  return state;
}
