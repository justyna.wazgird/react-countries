import React, { Component } from 'react';
import CountrySelected from './country_selected';

const CountryShortList = () => {
    return (
      <div className="card-block">
        <CountrySelected />
      </div>
    );
};

export default CountryShortList;
