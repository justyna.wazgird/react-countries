import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchCountries } from "../actions/index";

class CountrySearch extends Component {
  constructor(props) {
    super(props);

    this.state = { term: "" };

    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onInputChange(event) {
    this.setState({ term: event.target.value });
  }

  onFormSubmit(event) {
    event.preventDefault();

    this.props.fetchCountries(this.state.term);
    this.setState({ term: "" });
  }

  render() {
    return (
      <div class="card-header">
        <form onSubmit={this.onFormSubmit} className="input-group">
          <input
            placeholder="Search for a country..."
            className="form-control"
            value={this.state.term}
            onChange={this.onInputChange}
          />
          <span className="input-group-btn">
            <button type="submit" className="btn btn-secondary">Submit</button>
          </span>
        </form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchCountries }, dispatch);
}

export default connect(null, mapDispatchToProps)(CountrySearch);
